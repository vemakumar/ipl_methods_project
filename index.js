
const csvtojson = require('csvtojson');
const fs = require('fs');

const match = './data/matches.csv'
const deliveries = './data/deliveries.csv';

const converter=(csvFilePath,name)=>{
csvtojson()
  .fromFile(csvFilePath)
  .then((jsonArray) => {
    // Now 'jsonArray' contains an array of JSON objects

    // If you want to write the JSON array to a file, you can do so
    fs.writeFileSync('./src/public/output/'+ String(name)+'.json',JSON.stringify(jsonArray, null, 2));
  })
  .catch((error) => {
    console.error('Error converting CSV to JSON:', error);
  });
}

const nameDelivery="deliveries"
const nameMatches='matches'
const matchesData=converter(match,nameMatches)
const deliveriesData=converter(deliveries,nameDelivery)



