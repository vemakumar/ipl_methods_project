const matchesData = require('./../public/output/matches.json');
const fs = require('fs');

function calculatePlayerOfTheMatchAwards(matchesData) {
    const playerAwardsMap = {};

    matchesData.forEach((match) => {
        const season = match.season;
        const playerOfTheMatch = match.player_of_match;

        playerAwardsMap[season] = playerAwardsMap[season] || {};
        playerAwardsMap[season][playerOfTheMatch] = (playerAwardsMap[season][playerOfTheMatch] || 0) + 1;
    });

    const highestAwardsPerSeason = {};
    for (const season in playerAwardsMap) {
        const playerAwards = playerAwardsMap[season];
        const maxAwards = Math.max(...Object.values(playerAwards));
        const playerWithMostAwards = Object.keys(playerAwards).find(player => playerAwards[player] === maxAwards);
        highestAwardsPerSeason[season] = { player: playerWithMostAwards, awards: maxAwards };
    }

    return highestAwardsPerSeason;
}

const playerOfTheMatchAwardsData = calculatePlayerOfTheMatchAwards(matchesData);
fs.writeFileSync('./../public/output/playerOfTheMatchAwards.json', JSON.stringify(playerOfTheMatchAwardsData, null, 2));
