const deliveriesData = require('./../public/output/deliveries.json');
const matchesData = require('./../public/output/matches.json');
const fs = require('fs');

function calculateTopEconomicalBowlers(deliveriesData, matchesData, year) {
    const economyMap = {};
    const matchesOfYear = matchesData.filter(match => match.season === year);
    const matchIdsOfYear = matchesOfYear.map(match => match.id);

    deliveriesData.forEach((delivery) => {
        const matchId = delivery.match_id;
        const bowler = delivery.bowler;
        const totalRuns = parseInt(delivery.total_runs);
        const isWide = delivery.wide_runs !== '0';
        const isNoBall = delivery.noball_runs !== '0';

        if (matchIdsOfYear.includes(matchId) && !isWide && !isNoBall) {
            economyMap[bowler] = economyMap[bowler] || { runs: 0, balls: 0 };
            economyMap[bowler].runs += totalRuns;
            economyMap[bowler].balls++;
        }
    });

    const economyRate = {};
    for (const bowler in economyMap) {
        const { runs, balls } = economyMap[bowler];
        economyRate[bowler] = (runs / (balls / 6)).toFixed(2);
    }

    const sortedBowlerStats = Object.entries(economyRate)
        .sort(([, economyA], [, economyB]) => economyA - economyB)
        .slice(0, 10)
        .reduce((obj, [key, value]) => {
            obj[key] = value;
            return obj;
        }, {});

    return sortedBowlerStats;
}

const topEconomicalBowlers = calculateTopEconomicalBowlers(deliveriesData, matchesData, '2015');
fs.writeFileSync('./../public/output/topEconomicalBowlers.json', JSON.stringify(topEconomicalBowlers, null, 2));
