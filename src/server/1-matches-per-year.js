const matchesData = require('./../public/output/matches.json');
const fs = require('fs');

console.log(matchesData);


const matchesPerYear = matchesData.reduce((acc, match) => {
    const year = match.season;

    if (!acc[year]) {
        acc[year] = 1;
    } else {
        acc[year]++;
    }
    return acc;
}, {});

console.log(matchesPerYear);

fs.writeFileSync('./../public/output/matchesperyear.json', JSON.stringify(matchesPerYear, null, 2));




