const matchesData = require('./../public/output/matches.json');
const fs = require('fs');

function calculateMatchesWonPerTeamPerYear(matchesData) {
    const matchesWonPerTeamPerYear = {};

    matchesData.forEach((match) => {
        const year = match.season;
        const winner = match.winner;

        if (!matchesWonPerTeamPerYear[year]) {
            matchesWonPerTeamPerYear[year] = {};
        }

        if (!matchesWonPerTeamPerYear[year][winner]) {
            matchesWonPerTeamPerYear[year][winner] = 1;
        } else {
            matchesWonPerTeamPerYear[year][winner]++;
        }
    });
    
    return matchesWonPerTeamPerYear;
   
}

const matchesWonPerTeamPerYearData = calculateMatchesWonPerTeamPerYear(matchesData);
fs.writeFileSync('./../public/output/matchesWonPerTeamPerYear.json', JSON.stringify(matchesWonPerTeamPerYearData, null, 2));
