const fs = require('fs');
const matchesData = require('./../public/output/matches.json');

function calculateTossAndMatchWins(matchesData) {
    const tossAndMatchWins = {};

    matchesData.forEach((match) => {
        const tossWinner = match.toss_winner;
        const matchWinner = match.winner;

        if (tossWinner === matchWinner) {
            if (!tossAndMatchWins[tossWinner]) {
                tossAndMatchWins[tossWinner] = 1;
            } else {
                tossAndMatchWins[tossWinner]++;
            }
        }
    });

    return tossAndMatchWins;
}

const tossAndMatchWinsData = calculateTossAndMatchWins(matchesData);
fs.writeFileSync('./../public/output/tossAndMatchWins.json', JSON.stringify(tossAndMatchWinsData, null, 2));
