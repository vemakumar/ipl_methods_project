const deliveriesData = require('./../public/output/deliveries.json');
const matchesData = require('./../public/output/matches.json');
const fs = require('fs');

function calculateExtraRunsConcededPerTeam(deliveriesData, matchesData) {
    const extraRunsConcededPerTeam = {};
    const matches2016 = matchesData.filter(match => match.season === '2016');
    const matchIds2016 = matches2016.map(match => match.id);

    deliveriesData.forEach((delivery) => {
        const matchId = delivery.match_id;
        const bowlingTeam = delivery.bowling_team;
        const extraRuns = parseInt(delivery.extra_runs);

        if (matchIds2016.includes(matchId)) {
            extraRunsConcededPerTeam[bowlingTeam] = (extraRunsConcededPerTeam[bowlingTeam] || 0) + extraRuns;
        }
    });

    return extraRunsConcededPerTeam;
}

const extraRunsConcededPerTeamData = calculateExtraRunsConcededPerTeam(deliveriesData, matchesData);
fs.writeFileSync('./../public/output/extraRunsConcededPerTeam.json', JSON.stringify(extraRunsConcededPerTeamData, null, 2));
