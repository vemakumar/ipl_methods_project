
const fs = require('fs');
const deliveriesData = require('./../public/output/deliveries.json');

function calculateDismissals(deliveriesData) {
    const dismissals = {};

    deliveriesData.forEach((delivery) => {
        const dismissedPlayer = delivery.player_dismissed;
        const dismissalKind = delivery.dismissal_kind;

        if (dismissalKind !== 'run out' && dismissalKind !== 'retired hurt') {
            dismissals[dismissedPlayer] = (dismissals[dismissedPlayer] || 0) + 1;
        }
    });

    return dismissals;
}

const dismissalsData = calculateDismissals(deliveriesData);
fs.writeFileSync('./../public/output/dismissals.json', JSON.stringify(dismissalsData, null, 2));
